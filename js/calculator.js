$(document).ready(function() {
  function sw() {
    for (i = 1; i <= 7; i++) {
    if ($('#c' + i).prop('checked')) {
      $('.form' + i).css('display' , 'block');
    } else {
      $('.form' + i).css('display' , 'none');
    }}
  };
  function sw2() {
    for (i = 1; i <= 3; i++) {
    if ($('#f1-' + i).prop('checked')) {
      $('.f2-' + i).css('display' , 'block');
      $('.mfigure' + i).css('display' , 'block');
      $('#c1').next().css('color' , 'transparent');
    } else {
      $('.f2-' + i).css('display' , 'none');
      $('.mfigure' + i).css('display' , 'none');
    }}
  };

$('.steps li').click(sw);
$('.form1 li').click(sw2);
//firefox memory
sw();
sw2();

$('.smoothscroll').on('click', function (e) {

e.preventDefault();

var target = this.hash,
  $target = $(target);

  $('html, body').stop().animate({
    'scrollTop': $target.offset().top
  }, 800, 'swing', function () {
    window.location.hash = target;
  });

});

});
